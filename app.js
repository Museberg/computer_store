const laptopsElement = document.getElementById("laptops");
const featuresElement = document.getElementById("features");
const laptopImageElement = document.getElementById("laptopImage");
const balanceElement = document.getElementById("balance");
const loanElement = document.getElementById("loan");
const payElement = document.getElementById("pay");
const laptopDescriptionElement = document.getElementById("laptopDescription");
const laptopPriceElement = document.getElementById("laptopPrice");

const getLoanButtonElement = document.getElementById("getLoan")
const workButtonElement = document.getElementById("work");
const bankButtonElement = document.getElementById("bank");
const repayButtonElement = document.getElementById("repayButton");
const buyLaptopButtonElement = document.getElementById("buyLaptop");


let laptops = [];
let loan = 0;
let balance = 500;
let totalPay = 0;

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops))

const addLaptopsToMenu = (laptops) => {
    laptops.forEach(x => addLaptopToMenu(x))

    // Setting up page
    const firstLaptop = laptops[0];
    changeLaptopFeatures(firstLaptop);
    changeLaptopImage(firstLaptop);
    changeLaptopDescription(firstLaptop);
    changeLaptopPrice(firstLaptop);
    balanceElement.innerText = balance + " DKK";
    loanElement.innerText = loan + " DKK";
    payElement.innerText = totalPay + " DKK";
}

const addLaptopToMenu = (laptop) => {
    const newLaptop = document.createElement("option");
    newLaptop.value = laptop.id;
    newLaptop.appendChild(document.createTextNode(laptop.title));

    laptopsElement.appendChild(newLaptop);
}

const handleLaptopMenuChange = e => {
    const selectedLaptop = laptops[e.target.selectedIndex];
    changeLaptopFeatures(selectedLaptop);
    changeLaptopImage(selectedLaptop);
    changeLaptopDescription(selectedLaptop);
    changeLaptopPrice(selectedLaptop);
}

const changeLaptopImage = (laptop) => {
    // Removing old picture
    laptopImageElement.innerHTML = "";
    const newImageElement = document.createElement("img");
    newImageElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + laptop.image;
    newImageElement.alt = "No image for laptop";
    newImageElement.width = "350";

    laptopImageElement.appendChild(newImageElement);
}

const changeLaptopFeatures = (laptop) => {
    // Clearing old laptop features
    featuresElement.innerHTML = "";
    laptop.specs.forEach(x => {
        const newListItem = document.createElement("li");
        newListItem.appendChild(document.createTextNode(x));

        featuresElement.appendChild(newListItem);
    });
}

const changeLaptopDescription = (laptop) => {
    laptopDescriptionElement.innerText = laptop.description;
}

const changeLaptopPrice = (laptop) => {
    laptopPriceElement.innerText = laptop.price + " DKK";
}


const handleLoan = () => {
    if (loan !== 0){
        alert("You need to pay out your old loan first!");
        return;
    }

    const loanWish = parseInt(prompt("Please enter the size of the loan you wish to get"));

    if (loanWish > (balance * 2)){
        alert("You cannot loan more than twice your current balance!");
        return;
    }

    loan = loanWish;
    balance += loan;
    balanceElement.innerText = balance + " DKK";
    loanElement.innerText = loan + " DKK";

    // Now that we have a loan, we must again show the repay button
    repayButtonElement.removeAttribute("hidden");
}

const handleBankButtonClick = () => {
    // If we have a loan, we must first deduct 10% and use that to pay off the loan
    if (loan > 0){
        loan -= totalPay * 0.1;
        balance += totalPay * 0.9;
        totalPay = 0;
    }
    if (loan === 0){
        balance +=  totalPay;
        totalPay = 0;
    }

    loanElement.innerText = loan + " DKK";
    payElement.innerText = totalPay + " DKK";
    balanceElement.innerText = balance + " DKK";
}

const handleRepayButtonClick = () => {
    if (totalPay > loan){
        totalPay -= loan;
        loan = 0;
    }
    else {
        loan -= totalPay;
        totalPay = 0;
    }

    loanElement.innerText = loan + " DKK";
    payElement.innerText = totalPay + " DKK";
}

const handleBuyLaptopButtonClick = () => {
    const laptopPrice = laptops[laptopsElement.selectedIndex].price;
    // If we cannot afford the laptop
    if (balance < laptopPrice){
        alert("You are poor. Go work!");
    }
    else {
        alert("You just bought a laptop!");

        balance -= laptopPrice;
        balanceElement.innerText = balance + " DKK";
    }
}

laptopsElement.addEventListener("change", handleLaptopMenuChange);
getLoanButtonElement.addEventListener("click", handleLoan);
workButtonElement.addEventListener("click", () => {
    totalPay += 100;
    payElement.innerText = totalPay + " DKK";
});
bankButtonElement.addEventListener("click", handleBankButtonClick);
repayButtonElement.addEventListener("click", handleRepayButtonClick);
buyLaptopButtonElement.addEventListener("click", handleBuyLaptopButtonClick);